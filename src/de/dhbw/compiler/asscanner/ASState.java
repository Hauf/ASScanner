package de.dhbw.compiler.asscanner;

/**
 * Created by marku on 07.04.2016.
 */
public enum ASState {
    WS,
    LSBR,
    RSBR,
    EOF,
    N,
    NU,
    NUL,
    NULL,
    COMMA,
    ZAHL,
    NAME,
    INVALID
}

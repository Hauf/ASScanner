/* **********************************************
 * Duale Hochschule Baden-W�rttemberg Karlsruhe
 * Prof. Dr. J�rn Eisenbiegler
 * 
 * Vorlesung �bersetzerbau
 * Praxis AS-Scanner tabellengesteuert
 * - Scanner-Klasse
 * 
 * **********************************************
 */

package de.dhbw.compiler.asscanner;

import java.io.Reader;

public class ASScanner {
		
	private Reader in = null;
	
	public ASScanner(Reader input) {
		this.in = input;
	}
	
	public Token nextToken() throws Exception {
		
		//TODO implement Scanner here ...
		ASState state = ASState.WS;
		Token t = null;
		while (t == null){
            char c = (char) in.read();
			switch (state){
                case WS:
                    switch (c){

                    }
                    break;
                case LSBR:
                    break;
                case RSBR:
                    break;
                case EOF:
                    break;
                case N:
                    break;
                case NU:
                    break;
                case NUL:
                    break;
                case NULL:
                    break;
                case COMMA:
                    break;
                case ZAHL:
                    break;
                case NAME:
                    break;
                case INVALID:
                    break;
                default:
                    break;
            }


		}



		return new Token(Token.INVALID, "");
	}
    private Token step(int c, ASState newState, boolean create, int newTokenType, Reader text) {
        Token res = null;
        if (create) {
            res = new Token(newTokenType, text.toString()); // Token erzeugen?
            text = new StringBuffer(); // text leeren?
        }
        if (c!=ignore) { text.append((char)c); } // Zeichen an text anhängen?
        state = newState; // Zustand fortschreiben?
        tokentype = newTokenType; // Neuer Token-Typ
        return res;
    }

}
